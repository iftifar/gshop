<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'typeId','username', 'email', 'password','remember_token','title','firstName','lastName','dob','sex','phoneNumber',
            'streetAddress','city','state','country','postcode'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //--Mutetors--//
    public function setTitleAttribute($value) {
        $this->attributes['title'] = ucfirst($value);
    }

    public function setFirstNameAttribute($value) {
        $this->attributes['firstName'] = ucfirst($value);
    }

    public function setLastNameAttribute($value) {
        $this->attributes['lastName'] = ucfirst($value);
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

    //--Relations--//
    public function userType() {
        return $this->belongsTo('App\UserType', 'typeId');
    }

    public function warehouses() {
        return $this->hasMany('App\Warehouse', 'managerId', 'id');
    }

    public function shops() {
        return $this->hasMany('App\Shop', 'managerId', 'id');
    }

    public function invoices() {
        return $this->hasMany('App\Invoice', 'createdBy', 'id');
    }

    public function shopRestockRequests() {
        return $this->hasMany('App\ShopRestockRequest', 'requestedBy', 'id');
    }


    //--Validation--//

    public function rules(){
        return[
        
        'typeId'=>'required', 
        'username'=>'required|max:255',
        'email'=>'required|email|max:255|unique:users',
        'password'=>'required||alphaNum|min:6',
        'remember_token'=>'required',
        'title'=>'required',
        'firstName'=>'required',
        'lastName'=>'required',
        'dob'=>'required',
        'sex'=>'required',
        'phoneNumber'=>'required',
        'streetAddress'=>'required',
        'city'=>'required',
        'state'=>'required',
        'country'=>'required',
        'postcode'=>'required'
        
        ];
    }
}
