<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
	//--Mutetors--//
    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }

    //--Relations--//
    public function user() {
        return $this->belongsTo('App\User', 'managerId');
    }

	public function shopStocks() {
		return $this->hasMany('App\ShopStock', 'shopId', 'id');
	}

    public function shopRestockRequests() {
        return $this->hasMany('App\ShopRestockRequest', 'shopId', 'id');
    }
}
