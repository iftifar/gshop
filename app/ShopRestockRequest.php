<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopRestockRequest extends Model
{
	//--Relations--//
    public function user() {
        return $this->belongsTo('App\User', 'requestedBy');
    }

    public function warehouse() {
        return $this->belongsTo('App\Warehouse', 'warehouseId');
    }

    public function shop() {
        return $this->belongsTo('App\Shop', 'shopId');
    }

    public function shopRestockRequestDetails() {
        return $this->hasMany('App\ShopRestockRequestDetail', 'shopRestockRequestId', 'id');
    }
}
