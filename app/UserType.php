<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;


class UserType extends Model
{
     use Notifiable;
     
     // For all variables to be mass assignable
	

	protected $fillable = [
	'name', 'remark'];

	//--Mutetors--//
    public function setNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }
	
	//Accessor
    public function getNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }

  	//--Relations--//
	public function users() {
		return $this->hasMany('App\User', 'typeId', 'id');
	}
	
	//--Validation--//

	public function rules(){
		return[
		'typename'=>'required',
		];
	}
}
