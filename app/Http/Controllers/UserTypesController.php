<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserType;
use Validator;
use Response;

class UserTypesController extends Controller
{
    public function index(){
    	return view('userTypeRegistrationPage');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, (new UserType)->rules());
        // return Validator::make($data, [
        //     'typename'=>'required'
        // ]);
    }

    public function store(Request $request){
         
         $userTypes= new UserType;
         $userTypes->name=$request->typename;
         $userTypes->remark=$request->remark;
         $userTypes->save();
            
            return "success";
        
         
        
    }
}
