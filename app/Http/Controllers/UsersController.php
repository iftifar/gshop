<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use Validator;
use Auth;
use Redirect;

class UsersController extends Controller
{
    public function index(){

    return view("UserCreatePage");	
    }

    protected function validator(array $data){
       
        return Validator::make($data, (new UserType)->rules());
        
    }



    public function getLoginPage(){

        return view('userLoginPage');
    }


    


    public function getlogout(){
            Auth::logout();
            return Redirect::to('home');
    }



}
