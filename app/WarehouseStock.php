<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseStock extends Model
{
    //--Relations--//
    public function warehouse() {
        return $this->belongsTo('App\Warehouse', 'warehouseId');
    }

    public function product() {
        return $this->belongsTo('App\Product', 'productId');
    }
}
