<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopRestockRequestDetail extends Model
{
    //--Relations--//
    public function shopRestockRequest() {
        return $this->belongsTo('App\ShopRestockRequest', 'restockRequestId');
    }

    public function product() {
        return $this->belongsTo('App\Product', 'productId');
    }
}
