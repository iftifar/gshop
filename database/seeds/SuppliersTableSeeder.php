<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\Supplier;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
		for($i=0; $i < 10; $i++){
			$supplier = new Supplier([
				'title' => $faker->title,
				'firstName' => $faker->firstName,
				'lastName' => $faker->lastName,
				'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
				'sex' => 'male',
				'phoneNumber' => $faker->phoneNumber,
				'secondaryPhoneNumber' => $faker->phoneNumber,
				'streetAddress' => $faker->streetAddress,
				'city' => $faker->city,
				'state' => 'Dhaka',
				'country' => $faker->country,
				'postcode' => $faker->postcode
			]);
			$supplier->save();
		}
    }
}
