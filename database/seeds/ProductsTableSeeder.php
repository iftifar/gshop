<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\Product;
use App\Brand;
use App\Category;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
		for($i=0; $i < 30; $i++){
			$product = new Product([
				'name' => $faker->unique()->name,
				'barCodeString' => str_random(random_int(2, 10)),
				'barCodeImageUrl' => str_random(random_int(20, 30)),
				'sku' => $faker->unique()->name,
				'attributes' => json_encode(['weight' => $faker->numberBetween(1,5).'kg', 'length' => $faker->numberBetween(1,5).'m','volume' => $faker->numberBetween(1,5).'l', 'color' => $faker->colorName]),
				'unitPrice' => $faker->randomFloat(2, 10, 100),
				'mrp' => $faker->randomFloat(2, 10, 100),
				'batchNo' => str_random(random_int(5, 10)),
				'manufactureDate' => $faker->date($format = 'Y-m-d', $max = 'now'),
				'expiryDate' => $faker->date($format = 'Y-m-d', $max = 'now'),
				'salesCodeString' => str_random(random_int(2, 10)),
				'salesCodeImageUrl' => str_random(random_int(20, 30))
			]);

			$brand = Brand::find(random_int(1, 5));
			$category = Category::find(random_int(1, 5));

			$product->brand()->associate($brand);
			$product->category()->associate($category);

			$product->save();
		}

		// access json data $<col name>-><key>
		// use in where cluse same
		// use in update cluse same
    }
}
