<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\ShopStock;
use App\Shop;
use App\Product;

class ShopStocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
		for($i=0; $i < 30; $i++){
			$shopStock = new ShopStock([
				'quantity' => $faker->randomFloat(2, 10, 100)
			]);

			$shop = Shop::find(random_int(1, 2));
			$product = Product::find(random_int(1, 30));

			$shopStock->shop()->associate($shop);
			$shopStock->product()->associate($product);

			// using try catch because of the 2 column unique key constrain.
			try{
				$shopStock->save();
			} catch(exception $e){
				$i--;
			}
		}
    }
}
