<?php

use Illuminate\Database\Seeder;

use App\Product;
use App\Supplier;

class ProductSupplierTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dbProducts = Product::all();
        $dbSuppliers = Supplier::all();

        foreach ($dbProducts as $dbProduct) {
        	$suppliers = [];
        	for($i = 0; $i < 2; $i++){
        		$supplier = Supplier::find(random_int(1, 10));
        		$suppliers[] = $supplier->id;
        	}
        	$dbProduct->suppliers()->syncWithoutDetaching($suppliers);
        }

        foreach ($dbSuppliers as $dbSupplier) {
        	$products = [];
        	for($i = 0; $i < 2; $i++){
        		$product = Product::find(random_int(1, 30));
        		$products[] = $product->id;
        	}
        	$dbSupplier->products()->syncWithoutDetaching($products);
        }
    }
}
