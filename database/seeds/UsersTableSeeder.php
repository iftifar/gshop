<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;
use App\User;
use App\UserType;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();
		for($i=0; $i < 10; $i++){
			$user = new User([
				'username' => $faker->unique()->name,
				'email' => $faker->unique()->safeEmail,
				'password' => '123',
				'remember_token' => str_random(10),
				'title' => $faker->title,
				'firstName' => $faker->firstName,
				'lastName' => $faker->lastName,
				'dob' => $faker->date($format = 'Y-m-d', $max = 'now'),
				'sex' => 'male',
				'phoneNumber' => $faker->phoneNumber,
				'streetAddress' => $faker->streetAddress,
				'city' => $faker->city,
				'state' => 'Dhaka',
				'country' => $faker->country,
				'postcode' => $faker->postcode
			]);
			$userType = UserType::find(random_int(1, 3));

			$user->userType()->associate($userType);

			$user->save();
		}
    }
}
