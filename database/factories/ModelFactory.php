<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
    	'typeId' => 1,
        'name' => $faker->unique()->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = '123',
        'remember_token' => str_random(10),
		'title' => $faker->title,
		'firstName' => $faker->firstName,
		'lastName' => $faker->lastName,
		'sex' => 'male',
		'phoneNumber' => $faker->phoneNumber,
		'streetAddress' => $faker->streetAddress,
		'city' => $faker->city,
		'state' => 'Dhaka',
		'country' => $faker->country,
		'postcode' => $faker->postcode
    ];
});



