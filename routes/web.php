<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('userTypesRegistration', 'UserTypesController@index');

Route::post('userTypesRegistration', 'UserTypesController@store');

Route::get('getlogout', 'UsersController@getlogout');

Route::get('getSupplierRegistrationPage', 'SuppliersController@getSupplierFormPage');

Route::post('registerSuppleir', 'SuppliersController@registerSupplier');

Route::get('getBrandRegistrationPage', 'SuppliersController@getBrandFormPage');

Route::post('registerBrand', 'SuppliersController@registerBrand');


Route::get('getCategoryRegistrationPage', 'CategoryController@getCategoryFormPage');

Route::post('Brand', 'SuppliersController@registerBrand');

