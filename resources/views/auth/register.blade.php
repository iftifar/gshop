@extends('layouts.master')

@section('body')
<div class="container" style="margin-top:50px">
   

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User Registration Form</div>
                    <div class="panel-body">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}
                   
                    <div class="form-group{{ $errors->has('typeId') ? ' has-error' : '' }}">
                    
                    <label for="typeId" class="col-md-4 control-label">Type Id</label>

                            <div class="col-md-6">
                            <input id="typeId" type="number" class="form-control" name="typeId" value="{{ old('typeId') }}" required>

                            @if ($errors->has('typeId'))
                            <span class="help-block">
                            <strong>{{ $errors->first('typeId') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div>
                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                    
                    <label for="username" class="col-md-4 control-label">username</label>

                            <div class="col-md-6">
                            <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required>

                            @if ($errors->has('username'))
                            <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    
                    <label for="email" class="col-md-4 control-label">email</label>

                            <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                            <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    
                    <label for="password" class="col-md-4 control-label">password</label>

                            <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required>

                            @if ($errors->has('password'))
                            <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('remember_token') ? ' has-error' : '' }}">
                    
                    <label for="remember_token" class="col-md-4 control-label">remember_token</label>

                            <div class="col-md-6">
                            <input id="remember_token" type="text" class="form-control" name="remember_token" value="{{ old('remember_token') }}" required>

                            @if ($errors->has('remember_token'))
                            <span class="help-block">
                            <strong>{{ $errors->first('remember_token') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    
                    <label for="title" class="col-md-4 control-label">title Name</label>

                            <div class="col-md-6">
                            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required>

                            @if ($errors->has('title'))
                            <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('firstName') ? ' has-error' : '' }}">
                    
                    <label for="firstName" class="col-md-4 control-label">firstName </label>

                            <div class="col-md-6">
                            <input id="firstName" type="text" class="form-control" name="firstName" value="{{ old('firstName') }}" required>

                            @if ($errors->has('firstName'))
                            <span class="help-block">
                            <strong>{{ $errors->first('firstName') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('lastName') ? ' has-error' : '' }}">
                    
                    <label for="lastName" class="col-md-4 control-label">lastName Name</label>

                            <div class="col-md-6">
                            <input id="lastName" type="text" class="form-control" name="lastName" value="{{ old('lastName') }}" required>

                            @if ($errors->has('lastName'))
                            <span class="help-block">
                            <strong>{{ $errors->first('lastName') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                    
                    <label for="dob" class="col-md-4 control-label">Date of Birth</label>

                            <div class="col-md-6">
                            <input id="dob" type="date" class="form-control" name="dob" value="{{ old('dob') }}" required>

                            @if ($errors->has('dob'))
                            <span class="help-block">
                            <strong>{{ $errors->first('dob') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                    
                    <label for="sex" class="col-md-4 control-label">sex </label>

                            <div class="col-md-6">
                            <input id="sex" type="text" class="form-control" name="sex" value="{{ old('sex') }}" required>

                            @if ($errors->has('sex'))
                            <span class="help-block">
                            <strong>{{ $errors->first('sex') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('phoneNumber') ? ' has-error' : '' }}">
                    
                    <label for="phoneNumber" class="col-md-4 control-label">phoneNumber Name</label>

                            <div class="col-md-6">
                            <input id="phoneNumber" type="text" class="form-control" name="phoneNumber" value="{{ old('phoneNumber') }}" required>

                            @if ($errors->has('phoneNumber'))
                            <span class="help-block">
                            <strong>{{ $errors->first('phoneNumber') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('streetAddress') ? ' has-error' : '' }}">
                    
                    <label for="streetAddress" class="col-md-4 control-label">street Address</label>

                            <div class="col-md-6">
                            <input id="streetAddress" type="text" class="form-control" name="streetAddress" value="{{ old('streetAddress') }}" required>

                            @if ($errors->has('streetAddress'))
                            <span class="help-block">
                            <strong>{{ $errors->first('streetAddress') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                    
                    <label for="city" class="col-md-4 control-label">city</label>

                            <div class="col-md-6">
                            <input id="city" type="text" class="form-control" name="city" value="{{ old('city') }}" required>

                            @if ($errors->has('city'))
                            <span class="help-block">
                            <strong>{{ $errors->first('city') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div><div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                    
                    <label for="state" class="col-md-4 control-label">state Name</label>

                            <div class="col-md-6">
                            <input id="state" type="text" class="form-control" name="state" value="{{ old('state') }}" required>

                            @if ($errors->has('state'))
                            <span class="help-block">
                            <strong>{{ $errors->first('state') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div>
                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">

                    <label for="country" class="col-md-4 control-label">country</label>

                            <div class="col-md-6">
                            <input id="country" type="text" class="form-control" name="country" value="{{ old('country') }}" >

                            @if ($errors->has('country'))
                            <span class="help-block">
                            <strong>{{ $errors->first('country') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div>
                    <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">

                            <label for="postcode" class="col-md-4 control-label">postcode
                            </label>

                            <div class="col-md-6">
                            <input id="postcode" type="text" class="form-control" name="postcode" value="{{ old('postcode') }}" >

                            @if ($errors->has('postcode'))
                            <span class="help-block">
                            <strong>{{ $errors->first('postcode') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div>
                    <div class="col-md-6">
                    <button class="btn btn-warning" type="submit"> Submit 
                    </button>
                    </div>

                </form>
                
                </div>
            </div>
        </div>
    </div>

   

     
</div>
@endsection
