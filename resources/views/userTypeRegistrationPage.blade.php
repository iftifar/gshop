@extends('layouts.master')

@section('title')
    User Type Registration
@endsection


@section('body')

<div class="container" style="margin-top:50px">
   

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User Type Registration Form</div>
                    <div class="panel-body">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/userTypesRegistration') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('typename') ? ' has-error' : '' }}">
                    
                    <label for="typename" class="col-md-4 control-label">Type Name</label>

                            <div class="col-md-6">
                            <input id="typename" type="text" class="form-control" name="typename" value="{{ old('typename') }}" required>

                            @if ($errors->has('typename'))
                            <span class="help-block">
                            <strong>{{ $errors->first('typename') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div>

                    <div class="form-group{{ $errors->has('remark') ? ' has-error' : '' }}">

                    <label for="remark" class="col-md-4 control-label">Remarks</label>

                            <div class="col-md-6">
                            <input id="remark" type="text" class="form-control" name="remark" value="{{ old('remark') }}" >

                            @if ($errors->has('remark'))
                            <span class="help-block">
                            <strong>{{ $errors->first('remark') }}</strong>
                            </span>
                            @endif
                            </div>
                    </div>
                    
                    <div class="col-md-6">
                    <button class="btn btn-warning" type="submit"> Submit 
                    </button>
                    </div>

                </form>
                
                </div>
            </div>
        </div>
    </div>

   

     
</div>
@endsection