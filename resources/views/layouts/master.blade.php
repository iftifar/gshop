<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>@yield('title')</title>
         <meta name="_token" content="{{ csrf_token() }}">
        <link href="{{URL::to('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
      
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="{{ URL::to('css/header.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ URL::to('css/footer.css') }}" rel="stylesheet" type="text/css">
         <link href="{{ URL::to('css/custom.css') }}" rel="stylesheet" type="text/css">
        @yield('styles')


    </head>
    <body>
        <div class="container">
         @include('includes.header')
          
          <main>
         @yield('body')     
          </main>  
         
            
         @include('includes.footer')
        </div>
        
        <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
         
        <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        
      @yield('scripts')
    </body>
</html>