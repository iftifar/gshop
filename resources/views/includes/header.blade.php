<header>
<div class="menu">
    <div class="container">
    <div class="navbar-header">
      <a href="{{url('/')}}"><b>GSHOP</b></a>
    </div>
    <div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ url('/register') }}"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="{{ url('/login') }}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        
        @if(Auth::check())
            <li><a href="{{ url('/getlogout') }}"><span class="glyphicon glyphicon-log-in"></span> logout</a></li>
        @endif
      </ul>
    </div>
  </div>
</div>
</header>